#ifndef MAIN_CPP_MENU_TRIANTREE_HPP
#define MAIN_CPP_MENU_TRIANTREE_HPP

#include "trianTree.h"
#include <iostream>
#include <string>

void Trian_Test() {
    int arr[20];
    for (int i = 0; i < 20; i++) {
        arr[i] = 2*i + 1;
    }

    TrianTree<int> * tree = new TrianTree<int>(arr, 20);
    string a;
    int type[5]{3, 1, 4, 2, 5};
    int type_2[5]{5, 2, 4, 1, 3};
    cout<<"Our tree:"<<endl;
    tree->Print(type);
    cout<<endl;
    cout<<"Our tree:"<<endl;
    tree->Print(type_2);
    cout<<endl;

    cout<<"We have {20}?:"<<endl;
    cout<<tree->search(20);
    cout<<endl;

    tree->insert(20);
    cout<<"Our tree + {20}:"<<endl;
    tree->Print(type);
    cout<<endl;
    cout<<"Our tree + {20}:"<<endl;
    tree->Print(type_2);
    cout<<endl;

    cout<<"We have {20}?:"<<endl;
    cout<<tree->search(20);
    cout<<endl;

    tree->deleteNode(20);
    cout<<"Our tree - {20}:"<<endl;
    tree->Print(type);
    cout<<endl;
    cout<<"Our tree - {20}:"<<endl;
    tree->Print(type_2);
    cout<<endl;


}

#endif //MAIN_CPP_MENU_TRIANTREE_HPP
