#include <iostream>
#include <string>
//#include "functions.h"
using namespace std;

template <typename T>
class TrianTree {
private:

	class Node {
	public:
		Node* Left;
		Node* Central;
		Node* Right;
		T ValueL;
		T ValueR;
	};


	Node* max(Node* ptr) {// node � ������������ ��������� (����� ������)
		if (ptr->Right == nullptr) {
			return ptr;
		}
		return max(ptr->Right);
	}

	Node* min(Node* ptr) {// node � ����������� ��������� (����� �����)
		if (ptr->Left == nullptr) {
			return ptr;
		}
		return min(ptr->Left);
	}

//--------------------------------------------------------------- methods for score --------------
		// ��������
	Node* deleteNodeRoot(T value, Node* ptr) {
		if (ptr != nullptr) {
			if (ptr->ValueL == value ) {
				if (ptr->Left) {
					Node* tmp = new Node;
					T tmpValue;
					tmp = max(ptr->Left);
					if (tmp->ValueL < tmp->ValueR) {
						tmpValue = tmp->ValueR;
						ptr = deleteNodeRoot(tmp->ValueR, ptr);
					}
					else {
						tmpValue = tmp->ValueL;
						ptr = deleteNodeRoot(tmp->ValueL, ptr);
					}
					ptr->ValueL = tmpValue;
				}
				else if (ptr->Central) {
					Node* tmp = new Node;
					T tmpValue;
					tmp = min(ptr->Central);
					if (tmp->ValueL > tmp->ValueR && tmp->ValueR != NULL) {
						tmpValue = tmp->ValueR;
						ptr = deleteNodeRoot(tmp->ValueR, ptr);
					}
					else {
						tmpValue = tmp->ValueL;
						ptr = deleteNodeRoot(tmp->ValueL, ptr);
					}
					ptr->ValueL = tmpValue;
				}
				else if (ptr->Right) {
					Node* tmp = new Node;
					T tmpValue;
					tmp = min(ptr->Right);
					if (tmp->ValueL > tmp->ValueR && tmp->ValueR != NULL) {
						tmpValue = tmp->ValueR;
						ptr = deleteNodeRoot(tmp->ValueR, ptr);
					}
					else {
						tmpValue = tmp->ValueL;
						ptr = deleteNodeRoot(tmp->ValueL, ptr);
					}
					ptr->ValueL = ptr->ValueR;
					ptr->ValueR = tmpValue;
				}
				else{
					ptr->ValueL = ptr->ValueR;
					ptr->ValueR = NULL;
					if (ptr->ValueL == NULL) {
						delete ptr;
						return nullptr;
					}
					return ptr;
				}
				
			}
			if (ptr->ValueR == value) {
				
				if (ptr->Right) {
					Node* tmp = new Node;
					T tmpValue;
					tmp = min(ptr->Right);
					if (tmp->ValueL > tmp->ValueR) {
						tmpValue = tmp->ValueR;
						ptr = deleteNodeRoot(tmp->ValueR, ptr);
					}
					else {
						tmpValue = tmp->ValueL;
						ptr = deleteNodeRoot(tmp->ValueL, ptr);
					}
					ptr->ValueR = tmpValue;
				}
				else if (ptr->Central) {
					Node* tmp = new Node;
					T tmpValue;
					tmp = max(ptr->Central);
					if (tmp->ValueL < tmp->ValueR) {
						tmpValue = tmp->ValueR;
						ptr = deleteNodeRoot(tmp->ValueR, ptr);
					}
					else {
						tmpValue = tmp->ValueL;
						ptr = deleteNodeRoot(tmp->ValueL, ptr);
					}
					ptr->ValueR = tmpValue;
				}
				else if (ptr->Left) {
					Node* tmp = new Node;
					T tmpValue;
					tmp = max(ptr->Left);
					if (tmp->ValueL < tmp->ValueR) {
						tmpValue = tmp->ValueR;
						ptr = deleteNodeRoot(tmp->ValueR, ptr);
					}
					else {
						tmpValue = tmp->ValueL;
						ptr = deleteNodeRoot(tmp->ValueL, ptr);
					}
					ptr->ValueR = ptr->ValueL;
					ptr->ValueL = tmpValue;
				}
				else {
					ptr->ValueR = NULL;
					if (ptr->ValueL == NULL) {
						delete ptr;
						return nullptr;
					}
					return ptr;
				}
			}

			if (value < ptr->ValueL) {
				ptr->Left =  deleteNodeRoot(value, ptr->Left);
			}
			else if (value > ptr->ValueR) {
				ptr->Right =  deleteNodeRoot(value, ptr->Right);
			}
			else if (ptr->ValueL < value && value < ptr->ValueR) {
				ptr->Central = deleteNodeRoot(value, ptr->Central);
			}
		}
		return ptr;
	}

	// �������
	Node* insertRoot(T value, Node* ptr) {
		if (ptr == nullptr) {
			ptr = new Node;
			ptr->ValueL = value;
			ptr->ValueR = NULL;
			ptr->Left = nullptr;
			ptr->Central = nullptr;
			ptr->Right = nullptr;
		}
		else if (!ptr->ValueR ) {
			if (ptr->ValueL < value) {
				ptr->ValueR = value;
			}
			else if (value != NULL) {
				T tmp = ptr->ValueL;
				ptr->ValueL = value;
				ptr->ValueR = tmp;
			}
		}
		else if ((ptr->ValueL == value || ptr->ValueR == value) && value != NULL) {
			cout << "the value has already been used " << endl;
			cout << value << endl;
		}
		else if (value < ptr->ValueL) {
			ptr->Left = insertRoot(value, ptr->Left);
		}
		else if (ptr->ValueL < value && value < ptr->ValueR) {
			ptr->Central = insertRoot(value, ptr->Central);
		}
		else if (value > ptr->ValueR) {
			ptr->Right = insertRoot(value, ptr->Right);
		}
		return ptr;
	}

	// �����
	bool searchNodeRoot(T value, Node* ptr) {
		if (ptr == nullptr) {
			return false;
		}
		if (ptr->ValueL == value || ptr->ValueR == value) {
			return true;
		}
		if (value < ptr->ValueL) {
			return searchNodeRoot(value, ptr->Left);
		}
		else if (value > ptr->ValueR) {
			return searchNodeRoot(value, ptr->Right);
		}
		else if (ptr->ValueL < value && value < ptr->ValueR) {
			return searchNodeRoot(value, ptr->Central);
		}
	}


//-----------------------------------------------------------  END  of methods for score -------------

	void freeTree(Node* ptr) {
		if (ptr != nullptr) {
			freeTree(ptr->Left);
			freeTree(ptr->Central);
			freeTree(ptr->Right);
			delete ptr;
		}
	}

public:
	Node* Root;

	TrianTree() {
		Root = nullptr;
	}

	TrianTree(T* arr, int size) {
		Node* node = new Node;
		Root = node;
		if (arr[0] < arr[1]) {
			node->ValueL = arr[0];
			node->ValueR = arr[1];
		}
		else {
			node->ValueL = arr[1];
			node->ValueR = arr[0];
		}
		node->Left = nullptr;
		node->Central = nullptr;
		node->Right = nullptr;
		for (int i = 2; i < size ; i ++) {
			this->insert(arr[i]);
		}
	}

	void deleteNode(T value) {
		Root = deleteNodeRoot(value, Root);
		return;
	}

	void insert(T value) {
		Root = insertRoot(value, Root);
	}

    void Print(Node * ptr, int * type) {
        if (ptr == nullptr) {
            return;
        }
        for (int i = 0; i < 5; i++) {
            if(type[i] == 1) {
                cout<<ptr->ValueL<<" ";
            }
            if(type[i] == 2) {
                cout<<ptr->ValueR<<" ";
            }
            if(type[i] == 3) {
                Print(ptr->Left, type);
            }
            if(type[i] == 4) {
                Print(ptr->Central, type);
            }
            if(type[i] == 5) {
                Print(ptr->Right, type);
            }
        }
    }

    void Print(int * type) {
        Print(this->Root, type);
    }

	bool search(T value) {
		return searchNodeRoot(value, Root);
	}


	~TrianTree() {
		freeTree(Root);
	}

};