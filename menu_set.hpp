#ifndef MAIN_CPP_MENU_SET_HPP
#define MAIN_CPP_MENU_SET_HPP

#include "Set.hpp"
#include "AVLTree.hpp"
#include <iostream>

using namespace std;

enum Menu_set {
    insert_key = 1,
    remove_key,
    map,
    where,
    reduce,
    find_key,
    save_to_string,
    back
};

void SetMenu(){
    printf("1) Insert_key\n");
    printf("2) Remove key\n");
    printf("3) Map\n");
    printf("4) Where\n");
    printf("5) Reduce\n");
    printf("6) Find key\n");
    printf("7) Save to string\n");
    printf("8) Back\n");
}


int f12(int v) {
    return v*v;
}

bool f22(int v) {
    return v % 2;
}

int f32(int v1, int v2) {
    return v1 + v2;
}


void Setmenu() {
    Menu_set mn = back;
    int mn_1 = mn;
    int sequence[3];
    int value;

    Set<int>* tree = new Set<int>();

    do {
        SetMenu();
        cin>>mn_1;

        switch (mn_1) {
            case (insert_key):
                cout<<"Input key:\n";
                cin>>value;

                tree->Add(value);
                break;
            case (remove_key):
                cout<<"Input key:\n";
                cin>>value;

                tree->Remove(value);
                break;
            case (map):
                tree->Map(f12);
                cout<<endl;
                break;
            case (where):
                tree->Where(f22);
                cout<<endl;
                break;
            case (reduce):
                cout << tree->Reduce(f32, 0) << endl;
                cout<<endl;
                break;
            case (find_key):
                cout<<"Input key:\n";
                cin>>value;
                cout << tree->Find(value) << endl;
                break;
            case (save_to_string):
                cout<<"Type of round:\n1) Root\n2) Left\n3) Right\n";
                for (int i = 0; i < 3; i++) {
                    cin>>sequence[i];
                }
                cout<<endl;
                tree->Print(sequence);
                cout<<endl;
                break;
            case(back):
                break;
            default:
                cout<<"Repid input\n";
                break;
        }
    } while (mn_1 != 8);
}

void Set_test() {
    int items[15];
    for (int i = 0; i < 14; i++) {
        items[i] = 14 - i;
    }

    Set<int>* tree = new Set<int>(items, 14);

    int type[3]{2, 1 ,3};
    cout<<"Our tree:"<<endl;
        tree->Print(type);
    cout<<endl;
    cout<<"We have (10) as a key?:\n";

    cout << tree->Find(10) << endl;

    cout<<"Remove 10 from tree";
    tree->Remove(10);

    cout<<"Our tree:"<<endl;
    tree->Print(type);
    cout<<endl;
}



#endif //MAIN_CPP_MENU_SET_HPP
