#ifndef MAIN_CPP_MENU_AVL_HPP
#define MAIN_CPP_MENU_AVL_HPP

#include "AVLTree.hpp"
#include <iostream>

using namespace std;

enum Menu_AVL {
    insert_key_avl = 1,
    remove_key_avl,
    map_avl,
    where_avl,
    reduce_avl,
    find_key_avl,
    save_to_string_avl,
    back_avl
};

void AVLMenu(){
    printf("1) Insert_key\n");
    printf("2) Remove key\n");
    printf("3) Map\n");
    printf("4) Where\n");
    printf("5) Reduce\n");
    printf("6) Find key\n");
    printf("7) Save to string\n");
    printf("8) Back\n");
}

int f11(int v) {
    return v*v;
}

bool f21(int v) {
    return v%2;
}

int f31(int v1, int v2) {
    return v1 + v2;
}




void AVLmenu() {
    Menu_AVL mn = back_avl;
    int mn_1 = mn;
    int sequence[3];
    int value;

    AVLTree<int> *tree = new AVLTree<int>();
    AVLTree<int> *newtree = nullptr;
    do {
        AVLMenu();
        cin >> mn_1;

        switch (mn_1) {
            case (insert_key_avl):
                cout << "Input key:\n";
                cin >> value;

                tree->Insert(value);
                break;
            case (remove_key_avl):
                cout << "Input key:\n";
                cin >> value;

                tree->Remove(value);
                break;
            case (map_avl):
                newtree = tree->Map(f11);
                delete tree;
                tree = newtree;
                cout << endl;
                break;
            case (where_avl):
                newtree = tree->Where(f21);
                delete tree;
                tree = newtree;
                cout << endl;
                break;
            case (reduce_avl):
                cout << tree->Reduce(f31, 0) << endl;
                cout << endl;
                break;
            case (find_key_avl):
                cout << "Input key:\n";
                cin >> value;
                cout << tree->Find(value) << endl;
                break;
            case (save_to_string_avl):
                cout << "Type of round:\n1) Root\n2) Left\n3) Right\n";
                for (int i = 0; i < 3; i++) {
                    cin >> sequence[i];
                }
                cout << endl;
                tree->Print(sequence);
                cout << endl;
                break;
            case (back_avl):
                break;
            default:
                cout << "Repid input\n";
                break;
        }
    } while (mn_1 != 8);
}
void AVL_Test() {

    AVLTree<int>* tree = new AVLTree<int>();
    for (int i = 0; i < 14; i++) {
        tree->Insert(14-i);
    }


    int type[3]{2, 1 ,3};
    cout<<"Our tree:"<<endl;
    tree->Print(type);
    cout<<endl;
    cout<<"We have (10) as a key?:\n";

    cout << tree->Find(10) << endl;

    cout<<"Remove 10 from tree"<<endl;
    tree->Remove(10);

    cout<<"Our tree:"<<endl;
    tree->Print(type);
    cout<<endl;

    AVLTree<int> * newtree = tree->GetSubTree(11);
    cout<<"Our subtree with root 11:"<<endl;
    newtree->Print(type);
    cout<<endl;

    cout<<"We have tree in tree?\n";
    cout<<tree->SearchSubtree(tree)<<endl;

    cout<<"Our tree:"<<endl;
    tree->Print(type);
    cout<<endl;
}


#endif //MAIN_CPP_MENU_AVL_HPP