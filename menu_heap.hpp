#ifndef MAIN_CPP_MANU_HEAP_HPP
#define MAIN_CPP_MANU_HEAP_HPP
#include <iostream>
#include "BinaryHeap.hpp"
using namespace std;

enum Menu {
    Heap = 1,
    SET,
    AVL,
    Heap_test,
    SET_test,
    AVL_test,
    Trian_test,
    Finish
};

void mainMenu(){
    printf("1) Heap\n");
    printf("2) SET\n");
    printf("3) AVL\n");
    printf("4) Heap_test\n");
    printf("5) SET_test\n");
    printf("6) AVL_test\n");
    printf("7) Trian_test\n");
    printf("8) Exit\n");
}

enum Menu_heap {
    Insert_key = 1,
    Get_max,
    Find_key,
    Print_matrix,
    Print_string,
    Back
};

void HeapMenu(){
    printf("1) Insert_key\n");
    printf("2) Get_max\n");
    printf("3) Find_key\n");
    printf("4) Print_matrix\n");
    printf("5) Print_string\n");
    printf("6) Back\n");
}

void Heapmenu() {
    Menu_heap mn = Back;
    int mn_1 = mn;

    BinaryHeap<float>* heapFloat = new BinaryHeap<float>();
    float value;
    do {
        HeapMenu();
        cin>>mn_1;
        switch (mn_1) {
            case (Insert_key):
                cout<<"Input key:\n";
                cin>>value;

                heapFloat->AddElement(value);
                break;
            case (Get_max):

                cout<<heapFloat->Getmax()<<endl;
                break;
            case (Find_key):
                cout<<"Input key:\n";
                cin>>value;

                cout<<heapFloat->Find(value)<<endl;
                break;
            case (Print_matrix):
                heapFloat->OutHeap();
                break;
            case (Print_string):
                heapFloat->OutMatrix();
                break;
            default:
                cout<<"Repid input\n";
                break;
        }
    } while (mn_1 != 6);
}

void Heapmenu_test() {
    int items[15];
    for (int i = 0; i < 14; i++) {
        items[i] = 15 - i;
    }
    BinaryHeap<int> * heapInt = new BinaryHeap<int>(items, 14);

    heapInt->AddElement(24);
    cout<<"Our tree:\n";

    heapInt->OutHeap();

    cout<<"We found key(20)?\n";
    cout<<heapInt->Find(20)<<endl;

    cout<<"We found key(24)?\n";
    cout<<heapInt->Find(24)<<endl;

    cout<<"Our tree:\n";
    heapInt->OutHeap();

    cout<<"Max element:\n";
    cout<<heapInt->Getmax()<<endl;

    cout<<"Our tree:\n";
    heapInt->OutHeap();
}




#endif //MAIN_CPP_MANU_HEAP_HPP
