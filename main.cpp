#include <iostream>
#include "menu_heap.hpp"
#include "menu_AVL.hpp"
#include "menu_set.hpp"
#include "menu_TrianTree.hpp"

using namespace std;

int main() {

    Menu mn = Finish;
    int mn_1 = mn;

    do {

        mainMenu();
        cin>>mn_1;

        switch (mn_1) {
            case (Heap):
                Heapmenu();
                break;
            case (SET):
                Setmenu();
                break;
            case (AVL):
                AVLmenu();
                break;
            case (Heap_test):
                Heapmenu_test();
                break;
            case (SET_test):
                Set_test();
                break;
            case (AVL_test):
                AVL_Test();
                break;
            case (Trian_test):
                Trian_Test();
                break;
            case (Finish):
                break;
            default:
                cout<<"Repid input\n";
                break;
        }
    } while (mn_1 != 8);
    return 0;
}
